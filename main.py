from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
app = Flask(__name__)

@app.route("/")
def main():
    i = 1
    html = """<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8"/>
                <script type="text/javascript">
                    function range(i) {
                        return i>1 ? range(i-1).concat(i-1) : [];
                    }
                    function shuffle(a) {
                        for (let i = a.length; i; i--) {
                            let j = Math.floor(Math.random() * i);
                            [a[i - 1], a[j]] = [a[j], a[i - 1]];
                        }
                    }
                    var indexes = range(1714);
                    shuffle(indexes);
                    var begin = 0;
                    var limit = 30;
                </script>
                <title>Infinitos Malvados</title>
                <link type="text/css" rel="stylesheet" href="css/style.css"/>   
            </head>
        """
            
    html += """<body>
        <div class="infinitos-malvados" id="infinitos">
        <p id="ranger"></>
        <h1>Infinitos Malvados</h1>
        """
    for i in range(i,i+30):
        html += '<div class="um-malvados">'
        html += "<img src='static/img/logo_"+str(i)+".gif'/><br/>"
        html += "<img src='static/img/tira_"+str(i)+".gif'/>"
        html += "</div>"
        
    html += """
        <script type="text/javascript">
            window.onscroll = function(ev) {
                var infinitos = document.getElementById('infinitos');
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    for (var i = begin; i <= limit; i++) {
                       var outroMalvados = document.createElement("div");
                       outroMalvados.className = "um-malvados";
                       var logoMalvados = document.createElement("img");
                       logoMalvados.src = 'static/img/logo_' + indexes[i] + '.gif';
                       outroMalvados.appendChild(logoMalvados);
                       var imgMalvados = document.createElement("img");
                       imgMalvados.src = 'static/img/tira_' + indexes[i] + '.gif';
                       outroMalvados.appendChild(imgMalvados);
                       infinitos.appendChild(outroMalvados);
                    }
                begin = limit;
                limit = limit + 30;
                }
            };
        </script>       
        </div>
        </body>
        </html>
        """
    return html
if __name__ == "__main__":
    app.run(host='0.0.0.0',port=8080)

